export class Product{
    public pid: string;
    public pname: string;
    public pimgurl: string;
    public pdesc: string;
    public pprice: number;

    constructor(obj?: any){
        if (obj){
            this.pid = obj.pid;
            this.pname = obj.pname;
            this.pimgurl = obj.pimgurl;
            this.pdesc = obj.pdesc;
            this.pprice = obj.pprice;
        }
    }
}