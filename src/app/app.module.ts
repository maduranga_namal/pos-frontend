import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PosService } from './pos.service';
import { ProductComponent } from './product/product.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { ProductcatalogComponent } from './productcatalog/productcatalog.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    NavbarComponent,
    HomeComponent,
    ContactComponent,
    ProductcatalogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    PosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

