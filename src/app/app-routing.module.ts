import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { ProductcatalogComponent } from './productcatalog/productcatalog.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'products', component: ProductComponent},
  { path: 'about', component: ContactComponent },
  { path: 'catalog', component: ProductcatalogComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
