import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './model/product';

@Injectable({
  providedIn: 'root'
})
export class PosService {

  productdata:any;
  private posURL = 'https://posbackend-boot.herokuapp.com/products';

  constructor(private http: HttpClient
    ) { }
  
    getProducts(): Observable<Product[]>{
      return this.http.get<Product[]>(this.posURL);
    }

    search(q: string)  {
      // console.log(q)
      // this.productdata = this.http.get<Product[]>(this.posURL);
      // return this.productdata.find((c => c.pname=q))
      // //find((c => c.pname == q));
console.log(this.http.get<Product[]>(this.posURL))
    }

}
