import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product';
import { PosService } from '../pos.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: Product[];
  product: Product[];
  productvalue: string;
  selectedProduct: Product;
  productDis = false;
  quantity = 1;

  constructor(private posService: PosService) { }

  ngOnInit(): void {
    this.posService.getProducts().subscribe(products => this.products = products);
  }

  send(data) {
    if (data !== '') {
      // if (data.length > 1) {
      //   this.word = data + data;
      // }
      this.product = this.products.filter(p => p.pname.toLowerCase().includes(data.toLowerCase()));
      if (this.product.length === 0) {
        this.product = this.products.filter(p => p.pid.toLowerCase().includes(data.toLowerCase()));
      }
    }
    else {
      this.product = [];
    }
  }
  getId(id) {
    this.selectedProduct = this.products.find(p => p.pid === id);
    this.productDis = true;
    setTimeout(() => {
      this.productvalue = '';
      this.product = [];
    }, 1300);
  }

  decrease(): void {
    if (this.quantity > 0) {
      this.quantity -= 1;
    }
  }

  add(): void {
    this.quantity += 1;
  }
}
